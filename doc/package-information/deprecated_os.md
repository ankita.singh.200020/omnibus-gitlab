---
redirect_to: 'https://docs.gitlab.com/ee/administration/package_information/deprecated_os.html'
---

This document was moved to [another location](https://docs.gitlab.com/ee/administration/package_information/deprecated_os.html).

<!-- This redirect file can be deleted after 2022-03-28. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
